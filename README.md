# Ansible Fail2Ban Role
An ansible role for installing fail2ban.

Built upon the awesome work of René Moser, [resmo/ansible-fail2ban](https://github.com/resmo/ansible-role-fail2ban)

## Usage:

    ---
    - hosts: all
      roles:
      - fail2ban
